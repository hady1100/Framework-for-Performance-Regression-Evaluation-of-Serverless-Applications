import { Aws, Duration, RemovalPolicy, Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as S3 from 'aws-cdk-lib/aws-s3';
import * as S3n from 'aws-cdk-lib/aws-s3-notifications';
import * as path from "path";
export class BackendStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // lambda dependeence layer

    // const layer = new lambda.LayerVersion(this, 'DependencyLayer', {
    //   code: lambda.Code.fromAsset('../scripts/lambda-layer'),
    //   compatibleRuntimes: [lambda.Runtime.PROVIDED],
    //   license: 'Apache-2.0',
    //   description: 'A layer that installs node module for AWS Lambda',
    // });

    // Initilize Lambda function
    const k6Dockerfile = path.join(__dirname, "../");
    const awsCollectorDockerUpdated = path.join(__dirname, "../../aws-collector-updated/");
    const awsCollectorDocker = path.join(__dirname, "../../aws-collector/");
    // defines an AWS Lambda resource
    const awsCollectorLambda = new lambda.DockerImageFunction(this, 'DataCollectorHandler', {
      code: lambda.DockerImageCode.fromImageAsset(awsCollectorDocker),
      environment: {
        ACCESS_KEY_ID: `${process.env.AWS_ACCESS_KEY_ID}`,
        SECRET_ACCESS_KEY: `${process.env.AWS_SECRET_ACCESS_KEY}`,
        REGION: `${process.env.AWS_REGION}`,
      },
      timeout: Duration.minutes(15),
    });
    const awsCollectorLambdaUpdated = new lambda.DockerImageFunction(this, 'DataCollectorUpdatedHandler', {
      code: lambda.DockerImageCode.fromImageAsset(awsCollectorDockerUpdated),
      environment: {
        ACCESS_KEY_ID: `${process.env.AWS_ACCESS_KEY_ID}`,
        SECRET_ACCESS_KEY: `${process.env.AWS_SECRET_ACCESS_KEY}`,
        REGION: `${process.env.AWS_REGION}`,
        CLUSTER_TYPE: `${process.env.CLUSTER_TYPE}`,
        INFLUXDB_HOST: `${process.env.INFLUXDB_HOST}`,
        INFLUXDB_PORT: `${process.env.INFLUXDB_PORT}`,
        INFLUXDB_ORG: `${process.env.INFLUXDB_ORG}`,
        INFLUXDB_BUCKET: `${process.env.INFLUXDB_BUCKET}`,
        INFLUXDB_ADMIN_TOKEN: `${process.env.INFLUXDB_ADMIN_TOKEN}`,
        INFLUXDB_TABLE_INFRA: `${process.env.INFLUXDB_TABLE_INFRA}`,
        INFLUXDB_TABLE_FUNCTIONS: `${process.env.INFLUXDB_TABLE_FUNCTIONS}`,
        INFLUXDB_USERNAME: `${process.env.INFLUXDB_USERNAME}`,
        INFLUXDB_PASSWORD: `${process.env.INFLUXDB_PASSWORD}`,
        V1_DB_NAME: `${process.env.V1_DB_NAME}`,
        V1_RP_NAME: `${process.env.V1_RP_NAME}`,
        V1_AUTH_USERNAME: `${process.env.V1_AUTH_USERNAME}`,
        V1_AUTH_PASSWORD: `${process.env.V1_AUTH_PASSWORD}`,
        GRAFANA_USERNAME: `${process.env.GRAFANA_USERNAME}`,
        GRAFANA_PASSWORD: `${process.env.GRAFANA_PASSWORD}`,
      },
      timeout: Duration.minutes(15),
    });
    const monitorLambda = new lambda.DockerImageFunction(this, 'MonitorApplicationHandler', {
      code: lambda.DockerImageCode.fromImageAsset(k6Dockerfile),
      environment: {
        INFLUX_USER: `${process.env.INFLUX_USER}`,
        INFLUX_PASSWORD: `${process.env.INFLUX_PASSWORD}`,
        FunctionName: awsCollectorLambdaUpdated.functionArn,
        GRAFANA_TOKEN: `${process.env.GRAFANA_TOKEN}`,
        COMMIT_SHA: `${process.env.CI_COMMIT_SHA}`,
        COMMIT_TITLE: `${process.env.CI_COMMIT_TITLE}`,
        DASHBOARD_ID: `${process.env.DASHBOARD_ID}`,
      },
      timeout: Duration.minutes(15),
      // layers: [layer]
    });
    // Initilize s3 buckets
    const serverlessScriptBucket = new S3.Bucket(this, "ServerlessScriptBucket", {
      versioned: false,
      bucketName:"serverlessscriptbucket",
      publicReadAccess: false,
      removalPolicy: RemovalPolicy.DESTROY
    });

    // Grant access
    serverlessScriptBucket.grantReadWrite(monitorLambda);
    awsCollectorLambdaUpdated.grantInvoke(monitorLambda);

    // Add S3 ebent listener
    serverlessScriptBucket.addEventNotification(S3.EventType.OBJECT_CREATED, new S3n.LambdaDestination(monitorLambda));
  }
}
