const spawn = require("child_process").spawn;
const aws = require('aws-sdk');
const fs = require('fs');
const axios = require('axios');

console.log('Loading function');

const s3 = new aws.S3({
  apiVersion: '2006-03-01'
});

const lambda = new aws.Lambda({
  apiVersion: '2015-03-31'
});

exports.loadTest = async (event) => {

  const bucket = event.Records[0].s3.bucket.name;
  const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
  const params = {
    Bucket: bucket,
    Key: key,
  };

  try {
    const testScript = await s3.getObject(params).promise();
    console.log(testScript);
    console.log(testScript.Body.toString('utf-8'));
    fs.writeFileSync("/tmp/test.js", testScript.Body.toString('utf-8'), function (err) {
      if (err) {
        return console.log(err);
      }
      console.log("The file was saved!");
    });
  } catch (err) {
    console.log(err);
    const message = `Error getting object ${key} from bucket ${bucket}. Make sure they exist and your bucket is in the same region as this function.`;
    console.log(message);
    throw new Error(message);
  }

  const host = '138.246.236.156';
  const influx_db_port = '8086';
  const grafana_port = '3001';

  const response = await axios.get(`http://${host}:${grafana_port}/api/dashboards/uid/${process.env.DASHBOARD_ID}`, {
    headers: {
      Authorization: `Bearer ${process.env.GRAFANA_TOKEN}`
    }
  });

  const startDate = Date.now();


  const result = await new Promise(async (resolve, reject) => {
    console.log("Load test is starting.. ", JSON.stringify(event));


    let childK6Process = spawn("k6", [
      "run",
      `/tmp/test.js`,
      "--out",
      `influxdb=http://${process.env.INFLUX_USER}:${process.env.INFLUX_PASSWORD}@${host}:${influx_db_port}/k6db`,
    ]);

    childK6Process.stdout.on("data", function (data) {
      process.stdout.write(data.toString());
    });

    childK6Process.stderr.on("data", function (data) {
      process.stdout.write(data.toString());
    });

    childK6Process.on("close", function (code) {
      console.log("Finished with code " + code);
      console.log("Calling Data Collector" + process.env.FunctionName);
      lambda.invoke({
        FunctionName: `${process.env.FunctionName}`,
      }, (err, data) => {
        if (err) {
          console.log('Function execution failed');
          console.log(err);
          reject();
        } else {
          console.log(`executed function ${process.env.FunctionName}`);
          console.log(data);
          resolve();
        }
      });
    });

    childK6Process.on("error", function (code) {
      console.log("Error Occured", code);
      reject();
    });
  });
  const annotationsEndpoint = `http://${host}:${grafana_port}/api/annotations`;

  const endDate = Date.now();

  for (panel in response.data.dashboard.panels) {
    const annotationBody = {
      "dashboardId": response.data.dashboard.id,
      "panelId": panel.id,
      "time": startDate,
      "timeEnd": endDate,
      "tags": [`Lambdas updated`],
      "text": `A lambda function has has changed with commit sha ${process.env.COMMIT_SHA} and title ${process.env.COMMIT_TITLE}`,
    };
    await axios.post(annotationsEndpoint, annotationBody, {
      headers: {
        Authorization: `Bearer ${process.env.GRAFANA_TOKEN}`,
        "Content-Type": "application/json",
        "Accept": "application/json",
      }
    });
  }
  return result;
};