import sys
import DataCollector
import json
import asyncio
#!/usr/bin/env python
import yaml
import sys, getopt
from typing import List
import traceback
import asyncio

from Clusters import BaseCollector
from Clusters import GCFCollector
from Clusters import OpenWhiskCollector
from Clusters import AWSCollector
from datetime import datetime
from InfluxDBWriter import InfluxDBWriter

# TODO: for more accurate results, take start and end time as input.
aws_data_collector = AWSCollector()

influx_db_writer_obj = InfluxDBWriter("config.yaml")

def handler(event, context):
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(DataCollector.collect_from_clusters("config.yaml", 'aws', influx_db_writer_obj, aws_data_collector, [], True))
        return json.loads(json.dumps("write success", default=str))
    except:
        return json.loads(json.dumps("write FAILgit add", default=str))