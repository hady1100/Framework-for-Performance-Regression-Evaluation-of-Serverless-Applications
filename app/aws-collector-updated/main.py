#!/usr/bin/env python
from flask import Flask, Response, request
from typing import List
import asyncio
import os
from decouple import config
from Clusters import BaseCollector
from Clusters import OpenFaasCollector
from Clusters import OpenWhiskCollector
from Clusters import AWSCollector
from datetime import datetime
from PeriodicAsync import PeriodicAsyncThread
from InfluxDBWriter import InfluxDBWriter
import logging
import sys
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("/tmp/log.log"),
        logging.StreamHandler(sys.stdout)
    ]
)
#logging.basicConfig(filename='tmp/log.log', format='%(message)s', filemode='w', level=logging.DEBUG)
logger = logging.getLogger(__name__)

app = Flask(__name__)
loop = asyncio.get_event_loop()
apt = PeriodicAsyncThread(30)


class CollectData:
    def __init__(self) -> None:

        # INFLUXDB configuration
        self.influx_config = {
            "host": os.getenv('INFLUXDB_HOST'),
            "port": 8086,
            "token": os.getenv('INFLUXDB_ADMIN_TOKEN'),
            "org": os.getenv('INFLUXDB_ORG'),
            "bucket": os.getenv('INFLUXDB_BUCKET'),
            "table_functions": os.getenv('INFLUXDB_TABLE_FUNCTIONS'),
            "table_infra": os.getenv('INFLUXDB_TABLE_INFRA')
        }

        self.influx_db_writer_obj = InfluxDBWriter(self.influx_config)

        # Cluster Configuration
        self.cluster_type = os.getenv('CLUSTER_TYPE')
        self.cluster_name = os.getenv('CLUSTER_NAME')
        if self.cluster_type == "OPENFAAS":
            self.cluster_password = os.getenv('CLUSTER_AUTH')
            self.cluster_host = os.getenv('CLUSTER_HOST')
            self.cluster_username = os.getenv('CLUSTER_USERNAME')
            self.cluster_apigw_access_token = os.getenv('CLUSTER_API_GW_ACCESS_TOKEN')
            self.cluster_gateway_port = os.getenv('CLUSTER_GATEWAY_PORT')
            self.cluster_serverless_platform_prometheus_port = os.getenv(
                'CLUSTER_SERVERLESS_PLATFROM_PROMETHEUS_PORT')
            self.cluster_kubernetes_prometheus_port = os.getenv(
                'CLUSTER_KUBERNETES_PROMETHEUS_PORT')
            self.cluster_collector_obj = OpenFaasCollector("http://" + self.cluster_host + ':' + str(self.cluster_serverless_platform_prometheus_port),
                                                           "http://" + self.cluster_host + ':' + str(self.cluster_kubernetes_prometheus_port))

        elif self.cluster_type == "OPENWHISK":
            self.cluster_auth = os.getenv('CLUSTER_AUTH')
            self.cluster_host = os.getenv('CLUSTER_HOST')
            self.cluster_username = os.getenv('CLUSTER_USERNAME')
            self.cluster_apigw_access_token = os.getenv('CLUSTER_API_GW_ACCESS_TOKEN')
            self.cluster_host_port = os.getenv('CLUSTER_GATEWAY_PORT')
            self.cluster_api_host_port = self.cluster_host + ':' + str(self.cluster_host_port)
            self.cluster_serverless_platform_prometheus_port = os.getenv(
                'CLUSTER_SERVERLESS_PLATFROM_PROMETHEUS_PORT')
            self.cluster_kubernetes_prometheus_port = os.getenv(
                'CLUSTER_KUBERNETES_PROMETHEUS_PORT')
            self.cluster_collector_obj = OpenWhiskCollector("http://" + self.cluster_host + ':' + str(self.cluster_serverless_platform_prometheus_port),
                                                           "http://" + self.cluster_host + ':' + str(self.cluster_kubernetes_prometheus_port))
        elif self.cluster_type == "AWS":
            self.cluster_host = "AWS"
            self.cluster_collector_obj = AWSCollector()

    async def collect_from_clusters(self) -> None:
        dt = datetime.now()
        seconds = int(dt.strftime('%s'))
        print("About to collect data")
        print(self.cluster_type)
        print(os.getenv("cluster_type"))
        print(self.cluster_collector_obj)
        # logging.debug(f'cluster_host {self.cluster_host}')
        # if self.cluster_type == "OPENFAAS":
        #      logging.debug('cluster_host %s',self.cluster_host)
        #      logging.debug('seconds %s',str(seconds))
        #      logging.debug('cluster_password %s',self.cluster_password)
        #      logging.debug('cluster_username %s',self.cluster_username)
        #      logging.debug('cluster_gateway_port %s',self.cluster_gateway_port)
        #      logging.debug('cluster_serverless_platform_prometheus_port %s',self.cluster_serverless_platform_prometheus_port)
        #      logging.debug('cluster_kubernetes_prometheus_port %s',self.cluster_kubernetes_prometheus_port)

        data_list = await self.cluster_collector_obj.collect(self.cluster_name, seconds - 2*60*60*60, seconds)
        # logging.debug(f'cluster_type {self.cluster_type}')
        print('Data collected succesfully')
        print(data_list)
        if len(data_list) > 0:
            # logging.debug(f'data_list {data_list}')
            print("functions_usage" in data_list)
            print('===============================')
            print(self.influx_config)
            try:
                self.influx_db_writer_obj.write_dataframe_influxdb(
                    data_list, "functions_usage")
            except Exception as e:
                print(e)

            if "functions_usage" in data_list and not data_list["functions_usage"].empty:
                self.influx_db_writer_obj.write_dataframe_influxdb(
                    data_list, "functions_usage")
            if "system_usage" in data_list and not data_list["system_usage"].empty:
                self.influx_db_writer_obj.write_dataframe_influxdb(
                    data_list["system_usage"], "system_usage")

    async def collect_data(self) -> None:
        await self.collect_from_clusters()
        print("All deployment/removal finished")


async def collect_data_interface():
    collect_data_obj = CollectData()
    await collect_data_obj.collect_data()


@app.route('/start')
def start_collection():
    response = Response("Data collection has been started")

    @response.call_on_close
    def on_close():
        loop.create_task(apt.invoke_forever(collect_data_interface))
        loop.run_forever()

    return response


@app.route('/stop')
def stop_collection():
    loop.stop()
    return {
        "message": "data collection has been stopped",
    }


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=3005)
