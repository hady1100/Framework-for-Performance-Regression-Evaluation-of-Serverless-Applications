#!/usr/bin/env python
import asyncio
import json
import asyncio

from main import CollectData

def handler(event, context):
    try:
        collect_data_obj = CollectData()
        loop = asyncio.get_event_loop()
        loop.run_until_complete(collect_data_obj.collect_data())
        return json.loads(json.dumps("write success", default=str))
    except:
        return json.loads(json.dumps("write failed", default=str))