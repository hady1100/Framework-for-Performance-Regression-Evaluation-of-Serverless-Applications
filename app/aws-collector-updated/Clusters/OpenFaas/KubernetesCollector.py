#!/usr/bin/env python
from Clusters import BaseCollector
import logging
from pandas import DataFrame
from .PrometheusCollector import PrometheusCollector
from abc import abstractmethod

logging.basicConfig(filename='tmp/kubernetes_log_open_faas.log', format='%(message)s', filemode='w',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)


class KubernetesCollector(BaseCollector):

    def __init__(self, prometheus_url: str):
        self.prom_obj = PrometheusCollector(prometheus_url)

    async def collect_replicas(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects the amount of active replicas per function.
        Args:
            start:
                Integer - timestamp of start
            end:
                Integer - timestamp of end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - a Pandas Dataframe with the result.
        """
        query = "rate(kube_pod_info{pod=~\".*.*\", namespace=~\".*openfaas-fn.*\"}[1m])"
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "replicas", start, end, 60, True,
                                                     "pod")
        frame = frame.groupby(['timestamp', 'function_name'])[
            'replicas'].count().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_pods_cpu_sum(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects the amount of active replicas per function.
        Args:
            start:
                Integer - timestamp of start
            end:
                Integer - timestamp of end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - a Pandas Dataframe with the result.
        """
        query = "irate(container_cpu_usage_seconds_total{pod=~\".*.*\", namespace=~\".*openfaas-fn.*\"}[1m])"
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "pods-cpu-sum", start, end, 60, True,
                                                     "pod")
        print(frame)
        frame = frame.groupby(['timestamp', 'function_name'])[
            'pods-cpu-sum'].sum().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_pods_mem_sum(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects the amount of active replicas per function.
        Args:
            start:
                Integer - timestamp of start
            end:
                Integer - timestamp of end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - a Pandas Dataframe with the result.
        """
        query = "irate(container_memory_working_set_bytes{pod=~\".*.*\", namespace=~\".*openfaas-fn.*\"}[1m])"
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "pods-mem-sum", start, end, 60, True,
                                                     "pod")
        frame = frame.groupby(['timestamp', 'function_name'])[
            'pods-mem-sum'].sum().reset_index()
        frame['pods-mem-sum'] = frame['pods-mem-sum']  # / 2147483648
        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_pods_file_descp_sum(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects the amount of active replicas per function.
        Args:
            start:
                Integer - timestamp of start
            end:
                Integer - timestamp of end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - a Pandas Dataframe with the result.
        """
        query = "irate(container_file_descriptors{pod=~\".*.*\", namespace=~\".*openfaas-fn.*\"}[1m])"
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "pods-file-descp-sum", start, end, 60,
                                                     True,
                                                     "pod")
        frame = frame.groupby(['timestamp', 'function_name'])[
            'pods-file-descp-sum'].sum().reset_index()
        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame
    
    async def collect_pods_network_trasmit_bytes(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects the amount of active replicas per function.
        Args:
            start:
                Integer - timestamp of start
            end:
                Integer - timestamp of end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - a Pandas Dataframe with the result.
        """
        query = "irate(container_network_transmit_bytes_total{pod=~\".*.*\", namespace=~\".*openfaas-fn.*\"}[1m])"
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "pods-network-transmit-bytes", start, end, 60,
                                                     True,
                                                     "pod")
        if len(frame.values)>0:
            frame = frame.groupby(['timestamp', 'function_name'])[
                'pods-network-transmit-bytes'].sum().reset_index()
            frame.reset_index(inplace=True, drop=True)
            frame.set_index("timestamp", inplace=True)

        return frame
    
    async def collect_pods_network_receive_bytes(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects the amount of active replicas per function.
        Args:
            start:
                Integer - timestamp of start
            end:
                Integer - timestamp of end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - a Pandas Dataframe with the result.
        """
        query = "irate(container_network_receive_bytes_total{pod=~\".*.*\", namespace=~\".*openfaas-fn.*\"}[1m])"
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "pods-network-receive-bytes", start, end, 60,
                                                     True,
                                                     "pod")
        if len(frame.values)>0:
            frame = frame.groupby(['timestamp', 'function_name'])[
                'pods-network-receive-bytes'].sum().reset_index()
            frame.reset_index(inplace=True, drop=True)
            frame.set_index("timestamp", inplace=True)

        return frame
    
    async def collect_pods_fs_write_bytes(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects the amount of active replicas per function.
        Args:
            start:
                Integer - timestamp of start
            end:
                Integer - timestamp of end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - a Pandas Dataframe with the result.
        """
        query = "irate(container_fs_writes_bytes_total{pod=~\".*.*\", namespace=~\".*openfaas-fn.*\"}[1m])"
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "pods-fs-write-bytes", start, end, 60,
                                                     True,
                                                     "pod")
        if len(frame.values)>0:
            frame = frame.groupby(['timestamp', 'function_name'])[
                'pods-fs-write-bytes'].sum().reset_index()
            frame.reset_index(inplace=True, drop=True)
            frame.set_index("timestamp", inplace=True)

        return frame
    
    async def collect_pods_fs_read_bytes(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects the amount of active replicas per function.
        Args:
            start:
                Integer - timestamp of start
            end:
                Integer - timestamp of end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - a Pandas Dataframe with the result.
        """
        query = "irate(container_fs_reads_bytes_total{pod=~\".*.*\", namespace=~\".*openfaas-fn.*\"}[1m])"
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "pods-fs-read-bytes", start, end, 60,
                                                     True,
                                                     "pod")
        if len(frame.values)>0:
            frame = frame.groupby(['timestamp', 'function_name'])[
                'pods-fs-read-bytes'].sum().reset_index()
            frame.reset_index(inplace=True, drop=True)
            frame.set_index("timestamp", inplace=True)

        return frame


    async def collect_nodes_avg_cpu_usage_system(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        query = 'irate(node_cpu_seconds_total{instance=~\".*.*\", mode=\"system\"}[1m]) '
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "avg_cpu_system", start, end)

        frame = frame.groupby(['timestamp'])[
            'avg_cpu_system'].mean().reset_index()
        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_nodes_avg_cpu_usage_user(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        query = 'irate(node_cpu_seconds_total{instance=~\".*.*\", mode=\"user\"}[1m]) '
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "avg_cpu_user", start, end)

        frame = frame.groupby(['timestamp'])[
            'avg_cpu_user'].mean().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_nodes_avg_cpu_usage_iowait(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        query = 'irate(node_cpu_seconds_total{instance=~\".*.*\", mode=\"iowait\"}[1m]) '
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "avg_cpu_iowait", start, end)

        frame = frame.groupby(['timestamp'])[
            'avg_cpu_iowait'].mean().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_nodes_avg_cpu_usage_idle(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        query = 'irate(node_cpu_seconds_total{instance=~\".*.*\", mode=\"idle\"}[1m]) '
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "avg_cpu_idle", start, end)

        frame = frame.groupby(['timestamp'])[
            'avg_cpu_idle'].mean().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_avg_total_memory(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """

        query = 'node_memory_MemTotal_bytes{instance=~\".*.*\"}'
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "total_avg_mem_per_node", start, end)

        return frame

    async def collect_memory_avg_usage(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
            DataFrame - The result as df with the columns: 'timestamp', 'action' and 'invocations'
        """

        query = '(1 - (node_memory_MemAvailable_bytes{instance=~\".*.*\"} / ' \
                '(node_memory_MemTotal_bytes{instance=~\".*.*\"})))* 100'
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "avg_memory_usage_percent", start,
                                                     end)

        frame = frame.groupby(['timestamp'])[
            'avg_memory_usage_percent'].mean().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_bytes_transmitted(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
                    DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        interval = '1m'
        query = 'irate(container_network_transmit_bytes_total[{}])'.format(
            interval)

        frame = await self.prom_obj.query_prometheus(query, measurement_category, "network_bytes_transmitted", start,
                                                     end)

        frame = frame.groupby(['timestamp'])[
            'network_bytes_transmitted'].sum().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_disk_written_bytes(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
                    DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        interval = '1m'
        query = 'irate(node_disk_written_bytes_total[{}])'.format(interval)
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "disk_writes_bytes", start, end)

        frame = frame.groupby(['timestamp'])[
            'disk_writes_bytes'].mean().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)

        return frame

    async def collect_disk_read_bytes(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
                    DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        interval = '1m'
        query = 'irate(node_disk_read_bytes_total[{}])'.format(interval)
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "disk_read_bytes", start, end)

        frame = frame.groupby(['timestamp'])[
            'disk_read_bytes'].mean().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)
        return frame

    async def collect_disk_read_iops(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
                    DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        interval = '1m'
        query = 'irate(node_disk_reads_completed_total[{}])'.format(interval)
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "disk_read_iops", start, end)

        frame = frame.groupby(['timestamp'])[
            'disk_read_iops'].mean().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)
        return frame

    async def collect_disk_write_iops(self, start: int, end: int, measurement_category: str) -> DataFrame:
        """ Collects function invocations for FaaS functions.
        It uses PrometheusCollector's query_prometheus function.
        Args:
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            measurement_category:
                String - measurement_category
        Returns:
                    DataFrame - The result as Dataframe with the columns: 'timestamp', 'action' and 'invocations'
        """
        interval = '1m'
        query = 'irate(node_disk_writes_completed_total[{}])'.format(interval)
        frame = await self.prom_obj.query_prometheus(query, measurement_category, "disk_write_iops", start, end)

        frame = frame.groupby(['timestamp'])[
            'disk_write_iops'].mean().reset_index()

        frame.reset_index(inplace=True, drop=True)
        frame.set_index("timestamp", inplace=True)
        return frame

    @abstractmethod
    async def do_frame_postprocessing(self, frame: DataFrame, target_name: str, measurement_category: str) -> DataFrame:
        pass

    @abstractmethod
    async def collect(self, config_object: object, cluster_name: str, start: int, end: int) -> DataFrame:
        pass
