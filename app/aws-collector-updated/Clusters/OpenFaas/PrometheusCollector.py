#!/usr/bin/env python
from Clusters import BaseCollector
import logging
from pandas import DataFrame
import pandas as pd
from aiohttp_requests import requests
from abc import abstractmethod

logging.basicConfig(filename='/tmp/log.log',
                    format='%(message)s', filemode='w', level=logging.DEBUG)
logger = logging.getLogger(__name__)


class PrometheusCollector(BaseCollector):

    def __init__(self, prometheus_url: str):
        """ Collects usage data from the openwhisk cluster
        Args:
            prometheus_url:
                String - the url where we can find the prometheus instnace
        """

        self.prometheus_url = prometheus_url

        self.query_base = "/api/v1/query_range?query="

    @staticmethod
    def parse_result_to_dataframe(measurement_category: str, measurement_field_name: str, response,
                                  multiple_actions: bool = False,
                                  action_field: str = 'action') -> DataFrame:
        """ Parses a result from the prometheus instance to a DataFrame.
        Args:
            measurement_category:
                String - Name of the measurement category (=functions_usage, system_usage
            measurement_field_name:
                String - Name of the measurement field (=values from query result)
            response:
                Object - Parsed from the prometheus reponse
            multiple_actions:
                Boolean, optional - Indicates if the result contains measurement fields from different actions.
                Default: False
            action_field:
                String - action_field

        Returns:
            DataFrame - A DataFrame with the columns 'timestamp' and measurement_name (and action if
            multiple_actions is set)
        """

        result = response['data']['result']
        if len(result) == 0:
            return DataFrame()

        frame = DataFrame()

        if measurement_category == 'system_usage':
            frame = DataFrame(result[0]['values'], columns=['timestamp', measurement_field_name])

            frame[measurement_field_name] = frame[measurement_field_name].apply(
                lambda v: float(v))

        else:
            frames = []

            for metric in result:
                new_frame = DataFrame(metric['values'], columns=[
                                      'timestamp', measurement_field_name])

                if measurement_field_name == 'replicas' or measurement_field_name == 'pods-cpu-sum' or \
                        measurement_field_name == 'pods-mem-sum' or measurement_field_name == 'pods-file-descp-sum' or \
                        measurement_field_name == 'pods-network-transmit-bytes' or measurement_field_name == 'pods-network-receive-bytes' or \
                        measurement_field_name == 'pods-fs-write-bytes' or measurement_field_name == 'pods-fs-read-bytes':
                    if "pod" in metric['metric'].keys():
                        fun_name_arr = metric['metric']["pod"].split("-")
                        new_frame['function_name'] = '-'.join(
                            fun_name_arr[:-2]) + '.openfaas-fn'
                    else:
                        new_frame['function_name'] = "None"

                else:
                    if 'function_name' in metric['metric'].keys():
                        new_frame['function_name'] = metric['metric']['function_name']
                    elif 'faas_function' in metric['metric'].keys():
                        new_frame['function_name'] = metric['metric']['faas_function']
                    elif 'container' in metric['metric'].keys():
                        new_frame['function_name'] = metric['metric']['container']
                    else:
                        new_frame['function_name'] = "System"

                frames.append(new_frame)

            frame = pd.concat(frames, join="inner")

            frame[measurement_field_name] = frame[measurement_field_name].apply(
                lambda v: float(v))

        return frame

    @staticmethod
    def change_function_name(name):
        end = ".openfaas-fn"
        if name.endswith(end):
            return name.split(end)[0]
        return name

    def do_frame_postprocessing(self, frame: DataFrame, target_name: str, measurement_category: str) -> DataFrame:
        """ Performs postprocessing on dataframes.
        These are:
            - apply the target name
            - fill N/A values with 0
            - set timestamp as index
            - set the measurement_category (e.g. system resource, function usage)
        Args:
            frame:
                DataFrame - The frame which should be processed
            target_name:
                String - Name of the target
            measurement_category:
                String - Measurement category

        Returns:
            DataFrame - The processed DataFrame with the columns 'timestamp', 'target', 'measurement_category'
            and measurement fields(s) (and 'action' if applicable)
        """
        if frame.empty:
            return frame

        if "timestamp" in frame.columns:
            if measurement_category == 'system_usage':
                frame.reset_index(inplace=True, drop=True)
                frame.set_index("timestamp", inplace=True)
                frame.index = pd.to_datetime(frame.index, unit='s')
                frame['cluster_name'] = target_name
                frame['measurement_category'] = measurement_category
                # frame.fillna(0, inplace=True)
            else:
                frame = frame[frame.function_name != 'None'].copy()
                frame['function_name'] = frame.apply(lambda x: PrometheusCollector.change_function_name(x['function_name']),
                                                    axis=1)
                frame.reset_index(inplace=True, drop=True)
                #frame.drop_duplicates(subset=['timestamp', 'function_name'], keep='first', inplace=True)
                frame.set_index("timestamp", inplace=True)
                frame.index = pd.to_datetime(frame.index, unit='s')
                frame['cluster_name'] = target_name
                frame['measurement_category'] = measurement_category
                frame.fillna(0, inplace=True)

            return frame
        else:
            return DataFrame()

    async def query_prometheus(self, query: str, measurement_category: str, measurement_field_name: str, start: int,
                               end: int, step: int = 60,
                               multiple_actions: bool = False, action_field: str = "action") -> DataFrame:
        """ Queries the configured Prometheus instance with a given query.
        Args:
            query:
                String - The query that should be executed
            measurement_category:
                String - measurement_category
            measurement_field_name:
                String - Name of the measurement (=values from query result)
            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end
            step:
                Integer - step
            multiple_actions:
                Boolean, Optional - Indicates if the query returns measurement fields for multiple actions.
                Default: False
            action_field:
                String, Optional - The field that carries the action name. Default: "action"

        Returns:
            DataFrame - The processed DataFrame with the columns 'timestamp', 'target' and measurement_field_name(s)
            (and 'action' if multiple_actions is set)
        """
        url = self.prometheus_url + self.query_base + query + \
            "&start={}&end={}&step={}".format(start, end, step)
        # print(url)

        prometheus_request = await requests.get(url)

        # get result and parse
        if prometheus_request.status == 200:
            response = await prometheus_request.json()
            return self.parse_result_to_dataframe(measurement_category, measurement_field_name, response,
                                                  multiple_actions, action_field)

        return DataFrame()

    @abstractmethod
    async def collect(self, config_object: object, cluster_name: str, start: int, end: int) -> DataFrame:
        """ Collects one or more measurements for a Target from the configured Prometheus instance.

        Args:
            config_object:
                Object - The Target, for which the prometheus should be queried
            cluster_name:
                str - cluster_name

            start:
                Integer - A timestamp, where the query range should start
            end:
                Integer - A timestamp, where the query range should end

        Returns:
            DataFrame - Query result as DataFrame - with columns: 'timestamp', 'target', 'measurement_category',
            measurement fields(s) (+ 'action' if applicable)
        """
        pass
